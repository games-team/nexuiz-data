CHANGES since 2.4.2 to r6387:
compat-q3a: switch crylink/hagar, as crylink is closer to BFG and hagar is closer to Plasmagun
engine: add a half-baked "commandmode" (currently it has, like messagemode, no history, no line editing)
engine: added blend and orientation commands for effectinfo.txt (overrides the defaults chosen by the particle type)
engine: added cvar sv_gameplayfix_gravityunaffectedbyticrate - off by default
engine: added shader keywords dpshadow and dpnoshadow (no parameters), these modify the shadowing properties of a mesh, regardless of its other properties
engine: allow cl_maxfps and cl_maxidlefps to be 0, meaning unlimited (like in many QW clients)
engine: Blub's "deflate" extension to the download system. Currently only provides csprogs.dat in deflated form. Compatible in both directions, compression is only done if both client and server use this new code.
engine: capturing to ogg/theora video (cl_capturevideo_ogg; requires theora and vorbisenc dlls), ogg and avi capturing are now modules in the engine
engine: cl_demo_mousegrab
engine: csqc can get bone infos
engine: cvarlist now accepts wildcards
engine: demo recording: stuff csprogs.dat files into .dem files so demos can always be played back
engine: disable DGA mouse by default
engine: effectinfo: add "stretchfactor"
engine: fix a possible bug with zym model animation (this looks like a typo, and fixing it makes the shotgun anim in nexuiz appear more consistent)
engine: fix bugs with q1bsp/hlbsp rotating submodels
engine: fixed crash with ATI drivers on shutdown or vid_restart
engine: fixed pointsound in csqc
engine: fixed several collision bug regarding non solid players
engine: fix memory corruption when using trailparticles with an effect that has a dlight
engine: get rid of skinframe loading spam on dedicated servers
engine: if in menu, make con_closeontoggleconsole.integer also enable the toggleconsole key
engine: implemented occlusion query support on corona rendering, this enables coronas to fade according to the percentage of occluded pixels around the light origin, giving a more realistic corona behavior
engine: implemented zpass shadowing, not used yet
engine: model_brush: use the same tesselation number for patches in the same LOD group (q3map2 writes the same mins/maxs for any patch inside a LOD group, and this fact is used for identifying LOD groups)
engine: mod_q3bsp_nolightmaps: do not load lightmaps, use the bad q3map2-written vertex lighting instead (you have been warned). Mostly useful for maps with HUUUUGE lightmaps on low-end systems.
engine: new command by terrencehill: "status 1" and "status 2" as alternate, more readable status displays. "status" unchanged.
engine: new tcmod: "tcmod page <width> <height> <time>", as poor man's animmap alternative (stores all frames in ONE image in a <width>x<height> grid, and switches to the next frame every <time> seconds)
engine: open client sockets before video starts, so that the Windows Firewall prompt will occur before video starts (it tends to screw up graphics contexts otherwise)
engine: Q3BSP: only generate the shadow mesh if on a client. Skip that part on the dedicated server. Reduces load time and memory usage on a dedicated server.
engine: Q3's EF_TELEPORT_BIT added, to skip interpolation for a frame
engine: r_shadows 2: cast shadows always DOWN, ignore level lighting
engine: r_showsurfaces 3 "eeepc mode"
engine: showtopspeed variable added
engine: snd_spatialization_control: earphone friendly mode, reduces spatialization for distant objects
engine: sv_status_privacy", "0", "do not show IP addresses in 'status' replies to clients
engine: vid_netwmfullscreen, optional fullscreen mode for linux/glx allowing to alt-tab away from the game
game: airshot announcer for midair hits with the mortar
game: all projectiles and their effects are clientside now, massive bandwidth reduction
game: alternate HUD is now default
game: antilag improved
game: assault bugfixed
game: ballistics for bullets (but antilagged by default), that is all bullet guns (shotgun, machinegun, campingrifle) are affected by gravity and can pierce walls and players up to a certain thickness depending on gun)
game: better visual hint for unbalanced teams
game: bots can do bunnyhopping now
game: carrying strength and shield has less of a performance impact now
game: cl_gentle, clientside gib replacement for general audiences
game: cmd maplist improved
game: colors improved (TODO credit sev in nexuiz-credits.txt!)
game: command allready
game: command "cmd records"
game: command lockteams, unlockteams
game: command movetoteam_red, ...
game: command nospectators
game: command records
game: command vlogin must be used instead of vdo login now
game: command weapbest
game: crylink tweaked (bounce does half damage, damage lowered, primary consumes 2 cells)
game: ctf capture records now can only be made on an otherwise empty server, prevents "assisted" records
game: ctf flag touching when dead bug fixed
game: ctf scoring now uses flags, not frags (added several scoring options and configs)
game: custom weapon priorities for bots
game: cvar cl_casings
game: cvar cl_showpressedkeys
game: cvar cl_weaponpriority* (up to 10 custom weapon orders can be bound to a key)
game: cvar cl_weaponpriority_useforcycling (uses the menu set weapon priority for the normal next/prev commands)
game: cvar g_balance_ctf_damageforcescale
game: cvar g_balance_electro_combo_speed (secondary balls explode delayed, in a kind of chain reaction)
game: cvar g_bloodloss - a new pointless mutator ;)
game: cvar g_bugrigs*
game: cvar g_ctf_allow_drop
game: cvar g_ctf_fullbrightflags
game: cvar g_ctf_ignore_frags
game: cvar g_domination_point_fullbright
game: cvar g_instagib REMOVED (use arena mutator with minstanex)
game: cvar g_jump_grunt
game: cvar g_maxplayers (allows to create spectator slots)
game: cvar g_pickup_respawntime*
game: cvar g_pinata
game: cvar g_showweaponspawns (shows the location of guns on the maps if trying to switch to a gun that is not carried)
game: cvar g_spawn_furthest
game: cvar g_spawnpoints_auto_move_out_of_solid
game: cvar g_spawnshield_time automatically disables itself when shooting
game: cvar g_start_delay
game: cvar g_touchexplode
game: cvar g_warmup
game: cvar g_weaponreplace_*
game: cvar sv_clones
game: cvar sv_gentle
game: cvar sv_maxidle
game: cvar sv_ready_restart_after_countdown
game: cvar sv_ready_restart_nag
game: cvar sv_ready_restart_repeatable
game: cvar sv_vote_stop, the amount of time till one can vote again after stopping ones own vote
game: cvar teamplay_lockonrestart
game: cvar timelimit_overtime, timelimit_overtimes, timelimit_suddendeath (timelimit_maxovertime removed)
game: fix a server crash exploit
game: fix flag falling through bunker
game: fix q3skoredm1
game: fix some memory leaks
game: free camera for demo playback
game: friendly fire slightly added now
game: handle sv_foginterval on the client now
game: headshot announcer for the camping rifle
game: hook attaches to moving objects
game: hook can pull players (but detaches when that player shoots you)
game: hook detaches when teleporting
game: hook is no longer "seta"
game: hook jitter-less (full client side drawing)
game: hook sky-attach bug fixed
game: hook weapon consumes cell ammo
game: improved bot ai: teamworking, items rating, weapon combos, swimming, basic handling of ladders, better handling of jumpads, better detection of dangers, recognition of doors
game: improved weapon system, new weapons Port-O-Launch, MinstaNex, On-hand hook, TAG Seeker, HLAC, Camping Rifle; now up to 24 weapons supported
game: IP ban syncing support (servers can share a ban list, can be used as a kind of global IP ban system)
game: item respawn: 20s for 50health and 25armor
game: jetpack (g_jetpack, or entities item_jetpack, item_fuel, item_fuel_regen)
game: kick banned players also when they try to talk
game: lemmings countdown ('kill' and switching teams is no longer instant)
game: log can get :time: events
game: lots of weapon content changes, new models, new animations etc.. (shot origins)
game: map voting: map preview images to help identify the maps
game: map voting: nodetail 1 as default (does not show what others voted for)
game: more crosshairs added
game: more different death types
game: new dom points by ai
game: new music track by meoblast001 (TODO nexuiz-credits.txt)
game: new particle effects
game: new tokenizer fixing weirdly split kick reasons/vote commands
game: new weapon and player sounds
game: optimize bot waypoint loading
game: performance improved (fteqcc -O3)
game: race game type (see Docs/basics.html for more details)
game: ready now toggles the ready state
game: rocket launcher detonation fixed when button was pressed only for a very short time
game: scoreboard much improved
game: scores per-team
game: show the WelcomeMessage only on join for welcome_message_time seconds or when +show_info is pressed because many people did not know you can make this msg disappear with +zoom. add a line telling specs and observers about +show_info in the upper right
game: some voice command samples are used along with team messages
game: sounds: model specific player sounds
game: sv_foginterval
game: teamkill and typekill complaints and sounds
game: team radar (sv_cmd bbox, sv_cmd radarmap commands to create/debug it!)
game: texturecompression for lightmaps off (workaround for OS X bug)
game: timeout/in feature
game: turrets
game: vote call is now possible for server admins too
game: weapons rebalanced according to frag statistics
game: zoom unlagged, detect zoom scripts (for spectating/demos)
game: bots ai: added support for onslaught, complete rewrite of ctf ai, basic jetpack support, other minor fixes and improvements
game: file based bots configuration
game: bot scripting engine
game: weapon accuracy stats (enable with +showaccuracy)
map: aggressor supports keyhunt
map: all maps recompiled, now with external lightmaps which look way better
map: new maps accident, desertfactory, racetrack
mapping: added egyptsoc textures
mapping: allow mins/maxs specification for brush entities instead of models (as alternative)
mapping: armor_big (50 armor)
mapping: automatic shader generator script
mapping: entities.def updated
mapping: entity func_breakable
mapping: entity func_door: make DOOR_NOT_LINK work
mapping: entity func_door_rotating
mapping: entity func_pointparticles
mapping: entity func_sparks
mapping: entity func_train now supports -1 wait on path_corner, making it NOT wait
mapping: entity info_null now removes itself (as it should); this DOES break some maps that need fixing now
mapping: entity info_player_start now triggers other entities when someone spawns
mapping: entity misc_follow
mapping: entity misc_gamemodel, func_illusionary replacing misc_models
mapping: entity misc_laser changed and improved
mapping: entity target_spawn (BEWARE OF THE)
mapping: entity trigger_flipflop, trigger_monoflip, trigger_multivibrator
mapping: entity trigger_heal
mapping: entity trigger_hurt now can set a message2 as death message for push-kills
mapping: entity trigger_items
mapping: field nottargeted overrides targetname checks in e.g. func_door (so it can be used for misc_follow without needing an extra trigger)
mapping: field team on items, and cnt as weight
mapping: filtering by gametype: "gametypefilter" "+ctf,kh"
mapping: LOD for misc_models and similar entities
mapping: mapname.cfg is deprecated. Use clientsettemp_for_type and cdtrack in mapname.mapinfo
mapping: randomizing origin, angle or angles fields: "originjitter", "anglejitter", "anglesjitter" fields
mapping: target2, target3, target4 as extra target fields
mapping: team field in trigger_teleport and trigger_push
mapping: triggers now MUST be common/trigger; their exact shape now works (no longer their bbox)
mapping: turrets
mapping: zeroradiant support, bugfix patchset updated
map: reslimed: less distracting fog
map: some more TDM map settings
map: starship and silvercity: no CTF
media: new weapon system (see cl_weaponsystem.qc comment about model formats)
menu: blueplastic skin
menu: channel mixer
menu: cvar list editor
menu: demo browser
menu: game type in server browser
menu: input box margin now in chars, not percent
menu: more settings
menu: mouse fixed in -game pro (now havoc)
menu: multicampaign is back
menu: new playermodel previews (sev)
menu: play button in map info
menu: server favorites
menu: server info
menuskins: big buttons as extra graphics
menuskins: COLOR_SCROLLBAR_C
menu: skin selector
menuskins: HEIGHT_DIALOGBORDER
menu: weapon priority dialog
menu: wicked skins (7 different)
misc: +exec benchmark.cfg
misc: new color codes ^xRGB
models: new flag model
models: specop now knows that she is female
rcon2irc: disconnect fix
rcon2irc: irc_trigger
