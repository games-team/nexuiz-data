textures/flags/flag_red_cloth {
	cull none
	deformVertexes wave 100 sin 0 0.4 0 2.5
	{
		map textures/flags/flag_red_cloth.tga
	}
}

textures/flags/flag_red_laser {
	{
		map textures/flags/flag_red_laser.tga
		tcMod scroll 0.2 -1
		blendfunc add
	}
}


textures/flags/flag_blue_cloth {
	cull none
	deformVertexes wave 100 sin 0 0.4 0 2.5
	{
		map textures/flags/flag_blue_cloth.tga
	}
}

textures/flags/flag_blue_laser {
	{
		map textures/flags/flag_blue_laser.tga
		tcMod scroll 0.2 -1
		blendfunc add
	}
}

