#!/bin/sh

set -e

# See http://svn.icculus.org/nexuiz/tags/
RELEASE=2.5.2
SVN=8004

#rm -rf nexuiz-data-${RELEASE} nexuiz-data_${RELEASE}.orig.tar.gz
svn co -r${SVN} svn://svn.icculus.org/nexuiz/trunk \
	nexuiz-data-${RELEASE}

# Remove everything we're not interested in
#rm -rf nexuiz-data-${RELEASE}/misc/buildfiles/osx
#rm -rf nexuiz-data-${RELEASE}/misc/buildfiles/w32

# Use this to see what would be deleted and comment out the other find command!
#find -iname '.svn' -type d -exec echo rm -rf '{}' \;

tar --exclude-vcs --exclude=".svn" --exclude=".cvs*" \
	--exclude="misc/buildfiles/osx" \
	--exclude="misc/buildfiles/w32" \
	-czf nexuiz-data_${RELEASE}.orig.tar.gz \
	nexuiz-data-${RELEASE}
